//
//  Member+CoreDataClass.swift
//  CoreDataSample
//
//  Created by Максим Казаков on 07/05/2017.
//  Copyright © 2017 Максим Казаков. All rights reserved.
//

import Foundation
import CoreData

@objc(Member)
public class Member: NSManagedObject, EntityBase {
//    convenience init() {
        // Создание нового объекта
//        self.init(entity: CoreDataManager.instance.entityForName("Member"), insertInto: CoreDataManager.instance.managedObjectContext)
//    }
}
